'use strict';
import React from 'react';
import {renderToString} from 'react-dom/server';
import routes from './routes';
import { match, RoutingContext } from 'react-router'

export default function(req, res, next) {
    match({routes, location: req.url}, (error, redirectLocation, renderProps) => {
        if (error) {
            res.status(500).send(error.message)
        } else if (redirectLocation) {
            res.redirect(302, redirectLocation.pathname + redirectLocation.search)
        } else if (renderProps) {
            console.log('HEREEE');
            let markup = renderToString(<RoutingContext {...renderProps} />);
            res.render('index', {markup: markup});
        } else {
            res.status(404).send('Not found')
        }
    })
};
