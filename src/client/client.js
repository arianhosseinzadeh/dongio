import {render} from 'react-dom';
import React from 'react';
import routes from '../routes';

render(routes, document.getElementById('react-app'));