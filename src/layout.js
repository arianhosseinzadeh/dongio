import {render} from 'react-dom';
import React from 'react';
import { Link, IndexLink } from 'react-router'
import routes from './routes';

export default
class App extends React.Component {
    render() {
        console.log('Layout');
        let navbar = (<nav>
            <div>
                <ul>
                    <li>
                        <Link to='/login'>Login</Link>
                    </li>
                    <li>
                        <IndexLink to='/'>Home</IndexLink>
                    </li>
                    <li>
                        <Link to='/dashboard'>Dashboard</Link>
                    </li>
                </ul>
            </div>
        </nav>);
        return (
            <div>
                {navbar}
                {this.props.children}
            </div>
        )
    }
}