'use strict';
import React from 'react';
import {Route, IndexRoute, Router} from 'react-router';
import Home from './modules/home/components/home'
import Dashboard from './modules/dashboard/components/dashboard'
import Login from './modules/login/components/login'
import Layout from './layout';
export default(
    <Router>
        <Route path='/' component={Layout}>
            <Route component={Login} path='login'/>
            <IndexRoute component={Home}/>
            <Route component={Dashboard} path='dashboard'/>
        </Route>
    </Router>
);